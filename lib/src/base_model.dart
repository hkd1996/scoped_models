import 'package:scoped_model/scoped_model.dart';

class BaseModel extends Model {
  String _username="";
  String  _age="";
  String _department="";
  String get username => _username;
  String get age =>_age;
  String get department=>_department;

  void setUser(String username,String age, String department) {

    _username = username;
    _department=department;
    _age=age;
    // Notify listeners will only update listeners of state.
    notifyListeners();
  }



}